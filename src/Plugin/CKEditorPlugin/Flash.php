<?php

namespace Drupal\ckeditor_flash\Plugin\CKEditorPlugin;

use Drupal\editor\Entity\Editor;
use Drupal\ckeditor\CKEditorPluginBase;

/**
 * Defines the "ckeditor_flash" plugin.
 *
 * @CKEditorPlugin(
 *   id = "flash",
 *   label = @Translation("Flash"),
 *   module = "ckeditor_flash"
 * )
 */
class Flash extends CKEditorPluginBase {

  /**
   * Implements \Drupal\ckeditor\Plugin\CKEditorPluginInterface::getFile().
   */
  public function getFile() {
    return base_path() . 'libraries/flash/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return array();
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return array(
      'Flash' => array(
        'label' => t('Flash'),
        'image' => base_path() . 'libraries/flash/icons/flash.png',
      ),
    );
  }
  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return array();
  }
}