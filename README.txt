CKEditor Flash -  is an extension to the Drupal 8 CKEditormodule.

This plugin comes with a dialog to manage flash embed in the document, set standard and advanced properties (like scale, quality, alignment, background color, stylesheet classes). Flash files can be uploaded to the server and browsed if editor is intergrated with file manager (like CKFinder).


REQUIREMENTS
============
- ckeditor


INSTALLATION
============
1- Download the ckeditor_flash folder to your modules directory.
2- Download flash library(https://ckeditor.com/cke4/addon/flash) in the root libraries folder (/libraries).
3- Go to admin/modules and install the module.
4- Go to admin/config/content/formats and configure the desired profile.
5- Move a button into the Active toolbar.
6- Clear cache if required.
